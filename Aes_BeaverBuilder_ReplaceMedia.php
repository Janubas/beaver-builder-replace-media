<?php    
    /* Template Name: AOM2 Dev */

// JS for links scraping
// var str = $('body').html(); 
// var nstr = str.match(/\b(http|https):\/\/(staging.htgsolutions.com.au|staging.techwell.com.au).[^'"]+\.(?:jpg|jpeg|wav|png)/g);
// var links = Array.from(new Set(nstr));
// var out = '';
// for( var i in links ) {
//     out += '["'+ links[i] +'", "https://www.techwell.com.au/wp-content/uploads/2018/08/'+ links[i].substring(links[i].lastIndexOf('/')+1) +'"]__plssep_';
// }
// console.log(out);

// $replace_media = Aes_BeaverBuilder_ReplaceMedia::setup()
//              ->setVars([
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOM-Logo-TM_300x300.png", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOM-Logo-TM_300x300.png"],
//                 ["https://staging.techwell.com.au/wp-content/uploads/2017/09/logo-2.png", "https://www.techwell.com.au/wp-content/uploads/2018/08/logo-2.png"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC-Web-icon-three-150x150.png", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC-Web-icon-three-150x150.png"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC-Web-icon-two-150x150.png", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC-Web-icon-two-150x150.png"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC-Web-icon-one-150x150.png", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC-Web-icon-one-150x150.png"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/aom_realEstate-1200x800.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/aom_realEstate-1200x800.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/REMDE00003_1_.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/REMDE00003_1_.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/REMDE00001_V2_.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/REMDE00001_V2_.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/REMDE00004.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/REMDE00004.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Professional_One_Dave_-_Real_Estate-1.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Professional_One_Dave_-_Real_Estate-1.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Trade_One_Dave_-_Electrician-1.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Trade_One_Dave_-_Electrician-1.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_-_Melissa_-_Customer_Service_-_Seasonal_Offers-1.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_-_Melissa_-_Customer_Service_-_Seasonal_Offers-1.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Trade_Three_Loz_-_Coastal_Mowing.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Trade_Three_Loz_-_Coastal_Mowing.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Trade_Four_-_Plumbing_Emergency.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Trade_Four_-_Plumbing_Emergency.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/trades-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/trades-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/target-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/target-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_-_Dave_-_Customer_Service_-_24_Hour_Emerg_Service-1.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_-_Dave_-_Customer_Service_-_24_Hour_Emerg_Service-1.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Loz_Cust_Serv_Family_Owned.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Loz_Cust_Serv_Family_Owned.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Ruth_Customer_Serv_Special_Offers.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Ruth_Customer_Serv_Special_Offers.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Professional_One_Dave_-_Real_Estate.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Professional_One_Dave_-_Real_Estate.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Professional_Two_Mel_-_Recruitment_Sample.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Professional_Two_Mel_-_Recruitment_Sample.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Professional_Three_Loz_-_Freedom_Broking.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Professional_Three_Loz_-_Freedom_Broking.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Professional_Four_-_Insurance_Life.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Professional_Four_-_Insurance_Life.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/professional-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/professional-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/retail-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/retail-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Retail_One_Dave_-_Sales_Rep.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Retail_One_Dave_-_Sales_Rep.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Retail_Two_Shelley_-_PT_Sample.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Retail_Two_Shelley_-_PT_Sample.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Retail_Three_Loz_-_Everything_Tile.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Retail_Three_Loz_-_Everything_Tile.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Retail_Four_-_Fresh_Flowers.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Retail_Four_-_Fresh_Flowers.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Reps_One_Dave_-_Deals.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Reps_One_Dave_-_Deals.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Reps_Two_Melissa_-_After_Sales_Support.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Reps_Two_Melissa_-_After_Sales_Support.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Reps_Three_Loz_-_Reliability.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Reps_Three_Loz_-_Reliability.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Sales_Reps_Four_Ruth_-_Advice.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Sales_Reps_Four_Ruth_-_Advice.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/salesrep-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/salesrep-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/Education-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/Education-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Dave_Education_-_Health_and_Safety.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Dave_Education_-_Health_and_Safety.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Melissa_Education_-_Childrens_Tutoring.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Melissa_Education_-_Childrens_Tutoring.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Loz_Education_-_Driving.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Loz_Education_-_Driving.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Ruth_Education_-_First_Aid.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Ruth_Education_-_First_Aid.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Dave_Manufacturing_-_Sheds.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Dave_Manufacturing_-_Sheds.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Melissa_Manufacturing_-_Windows_and_Frames.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Melissa_Manufacturing_-_Windows_and_Frames.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Loz_Manufacturing_-_Steel_Fab.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Loz_Manufacturing_-_Steel_Fab.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Ruth_Manufacturing_-_Pool.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Ruth_Manufacturing_-_Pool.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/Manufacturing-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/Manufacturing-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/wholesale.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/wholesale.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Dave_Wholesale_-_Account_Management.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Dave_Wholesale_-_Account_Management.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Melissa_Wholesale_-_Reliable_Delivery.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Melissa_Wholesale_-_Reliable_Delivery.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Loz_Wholesale_-_Offers.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Loz_Wholesale_-_Offers.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Ruth_Wholesale_-_Range.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Ruth_Wholesale_-_Range.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Loz_Construction_-_Certifier.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Loz_Construction_-_Certifier.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Melissa_Construction_-_Architect.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Melissa_Construction_-_Architect.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Dave_Construction_-_Builder.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Dave_Construction_-_Builder.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Loz_Construction_-_Developer.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Loz_Construction_-_Developer.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/Contruction_Three-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/Contruction_Three-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/health2-1200x780.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/health2-1200x780.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Melissa_Health_and_Baeauty_-_Hairdresser.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Melissa_Health_and_Baeauty_-_Hairdresser.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Ruth_Health_and_Beauty_-_Massage.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Ruth_Health_and_Beauty_-_Massage.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Melissa_Health_and_Beauty_-_Personal_Trainer.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Melissa_Health_and_Beauty_-_Personal_Trainer.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOCT_Ruth_Health_and_Beauty_-_Salon-1.wav", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOCT_Ruth_Health_and_Beauty_-_Salon-1.wav"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC_Icon_One-1.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC_Icon_One-1.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC_Icon_Two-1.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC_Icon_Two-1.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC_Icon_Three-1.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC_Icon_Three-1.jpg"],
//                 ["https://staging.htgsolutions.com.au/wp-content/uploads/2018/06/AOC_Icon_Four-1.jpg", "https://www.techwell.com.au/wp-content/uploads/2018/08/AOC_Icon_Four-1.jpg"]
//              ]
//              )->replace(2282);

/**
 * This plugin finds and replaces strings from beaver builder post/page content.
 * 
 * @casestudy
 * Problem: Beaver builder's template export is not exporting and importing media to different a domain.
 * Solution: Find the media's url and replace to another domain's url
 *
 * @usage
 * $replace_media = Aes_BeaverBuilder_ReplaceMedia::setup()
 *                  ->setVars(
 *                      ['media url to be replaced 1', 'new media url 1'],
 *                      ['media url to be replaced 2', 'new media url 2']
 *                  )->replace($psot_id);
 *
 * @tags 
 * no ui|tool
 * 
 * @package Aes
 * @subpackage Aes/BeaverBuilder
 * @author Paul Edmund Janubas <pauledmundjanubas@gmail.com>
 */
class Aes_BeaverBuilder_ReplaceMedia
{
    /**
     * The instance
     * @var object
     */
    private static $instance;

    /**
     * The array of strings to find and replace
     * @var array
     */
    private $str_arr = [];

    /**
     * The post meta_key generated from beaver builder plugin
     * 
     * @var string
     */
    private $meta_key = '_fl_builder_data';

    /**
     * The post ID
     * 
     * @var int|boolean
     */
    private $post_id = false;

    /**
     * Singlton
     * 
     * @return object
     */
    public static function setup()
    {
        self::$instance = null;
        if( self::$instance === null ) {
            self::$instance = new Aes_BeaverBuilder_ReplaceMedia();
        }

        return self::$instance;
    }

    public function setVars( $vars = [] )
    {
        if( is_array($vars) && !empty( $vars ) ) {
            $this->str_arr = $vars;
        }

        return $this;
    }

    /**
     * Replce strings
     * 
     * @param  int|string
     * @return array
     */
    public function replace( $post_id = false )
    {
        if( !$post_id ) { return false; }

        $this->post_id = $post_id;

        $data = $this->getData();   

        $data = $this->replaceStrings( $data );
        $data = $this->updateData( $data );     

        return $data;
    }

    /**
     * Get serialized string from post_meta $this->meta_key
     * 
     * @return string
     */
    public function getData()
    {
        global $wpdb;

        $data = $wpdb->get_col( "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = '$this->meta_key' AND post_id = '$this->post_id'" );

        return $data;
    }

    /**
     * Update the updated serialized string to post_meta $this->meta_key
     * 
     * @param  string
     * @return string
     */
    public function updateData( $data )
    {
        global $wpdb;

        $wpdb->query( $wpdb->prepare( 
            "
            UPDATE $wpdb->postmeta 
            SET meta_value = %s
            WHERE meta_key = '$this->meta_key'
            AND post_id = '$this->post_id'
            ",
            $data 
        ));

        return $data;
    }

    /**
     * Perform search and replace string
     * 
     * @param  string
     * @return string
     */
    public function replaceStrings( $data )
    {
        if( empty($this->str_arr) ) { return $data; }

        $data = preg_replace('/(?=s:\d+:)/', '__plsrep__', $data[0]);
        $data = preg_split('/__plsrep__/', $data);

        foreach ($data as $key => $str) {

            foreach ($this->str_arr as $newstr) {

                if( strpos($data[$key], $newstr[0]) !== false ) {
                    
                    $rep    = str_replace($newstr[0], $newstr[1], $data[$key]);
                    preg_match('/(s:\d+:).+/s', $rep, $arr);
                    $strlen = strlen($arr[0]);
                    $arrlen = strlen($arr[1]);
                    $arrlen = ($strlen - $arrlen) - 3;
                    $count  = 's:'. $arrlen .':';
                    $rep    = preg_replace('/s:\d+:/', $count, $rep);

                    $data[$key] = $rep;
                }
            }
            
        }

        return implode('', $data);      
    }
}
?>